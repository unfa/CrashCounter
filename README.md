# CrashCounter

A streaming overlay that reacts to programs being shut down by displaying an explosion effect and incrementing a counter.

_This project is built with Godot 3 game engine._