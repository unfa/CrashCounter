extends Spatial

var running = false
var last = false
var crashes = 0

const TIMEOUT = 10.0

var timeout = 0.0

var app = ""

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func update_app():
	var out = []
	
	var new_app = ""
	
	OS.execute("pgrep", ["-i", "-a", "Ardour"], true, out)
	
	if out[0].length() > 0:
		new_app = "Ardour"
#		$Ardour.show()
#		$Zrythm.hide()
	
	OS.execute("pgrep", ["-i", "-a", "Zrythm"], true, out)

	if out[0].length() > 0:
		new_app = "zrythm"
#		$Zrythm.show()
#		$Ardour.hide()

	OS.execute("pgrep", ["-i", "-a", "Surge XT"], true, out)

	if out[0].length() > 0:
		new_app = "Surge XT"
#		$Zrythm.show()
#		$Ardour.hide()
	
	OS.execute("pgrep", ["-i", "-a", "BespokeSynth"], true, out)

	if out[0].length() > 0:
		new_app = "BespokeSynth"
#		$Zrythm.show()
#		$Ardour.hide()
	
	if new_app != "":
		if new_app != app:
			crashes = 0
			app = new_app
			$Control.hide()
			$Control/RichTextLabel.bbcode_text = app + " crashes: [b]" + str(crashes) + "[/b]"
	
	$Setup.play(app) # play the setup animation to change camera and text pos/scale

# Called when the node enters the scene tree for the first time.
func _ready():
	#get_viewport().transparent_bg = true
	pass
	
func _process(delta):
	
	#get_tree().get_root().set_transparent_background(true)
	
	if app == "":
		$Control/RichTextLabel.bbcode_text = ""
		return
	
	#update_app()
	
	if Input.is_action_just_pressed("ui_accept"):
		crashes = 0
		$Control/RichTextLabel.bbcode_text = app + " crashes: [b]" + str(crashes) + "[/b]"
	
	var out = []
	OS.execute("pgrep", ["-i", "-a", app], true, out)
	
	#print(out, out[0].length())
	
	if out[0].length() > 0:
		running = true
	else:
		running = false
	
	#if len(var2bytes(out)) < len(var2bytes(last)) and timeout == 0:
	if not running and last and timeout == 0:
		timeout = TIMEOUT
		crashes += 1
		$AnimationPlayer.play("Crash")
		$Control.show()
		$Control/RichTextLabel.bbcode_text = app + " crashes: [b]" + str(crashes) + "[/b]"
	
	
	last = running
	timeout = max(0, timeout - delta)
